// app/routes.js
var mongoose = require('mongoose');
var addUsers = require("./models/addUsers");

module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });


    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));


    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/profile', isLoggedIn, function(req, res) {
        addUsers.find({},function(err, users){
            res.render('profile.ejs', {
            user : req.user, // get the user out of session and pass to template
            users: users
        });
        })
    });
    //delete users from database 
   app.get('/profile/:naamDocent/delete', function(req, res){
    addUsers.remove({ naamDocent : req.params.naamDocent}, 
       function(err){
        if(err) res.json(err);
        else    res.redirect('/profile');
    });
});
   //change users status from aanwezig to afwezig or the way around
//    app.get('/profile/:naamDocent/wijzig', function(req, res){
//     addUsers.update({ naamDocent : req.params.naamDocent}, 
//     if (aanwezig == true) {{$set:{aanwezig: false}}} else{{$set:{aanwezig: true}}};,
//        function(err){
//         if(err) res.json(err);
//         else    res.redirect('/profile');
//     });
    
// });

   app.get('/profile/:naamDocent/wijzig', function(req, res){
var naamDocent = req.params.naamDocent;
addUsers.findOne({naamDocent: naamDocent}, function(err, user) {
    user.aanwezig = !user.aanwezig;
    user.save(function(err){
        if(err) res.json(err);
        else    res.redirect('/profile');
    });
});    
});
    // LOGOUT 
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    //graphs
    app.get('/graphs', function(req, res){
        res.render('graphs.ejs');
    })
    //add users page
    app.get('/addUsers', function(req, res){
        res.render('addUsers.ejs');
    })
    //add users from the add users page to the database
    app.post('/addUsers', function(req, res){
            new addUsers({
                naamDocent : req.body.naamDocent,
                emailDocent: req.body.emailDocent,
                aanwezig: req.body.aanwezig == undefined ? false : true
            }).save(function(err,doc){
                    if (err) res.render('unSuccesfullSignup.ejs')
                    else res.render('succesfullSignup.ejs')        
            })
});};




// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
