var mongoose = require('mongoose');
//define schema for new users

var addUsersSchema = mongoose.Schema({
            
        naamDocent    	 : String,
        emailDocent   	 : String,
        aanwezig		 : {type: Boolean, default: false} 
    });

//expose model to application

module.exports = mongoose.model('AddUsers', addUsersSchema);
