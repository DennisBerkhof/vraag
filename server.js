// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var router = express.Router();
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var sendgrid  = require('sendgrid')('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');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var hogan = require('hogan.js');
var fs = require('fs');
var configDB = require('./config/database.js');
var template = fs.readFileSync('./views/emailTemplate.hjs', 'utf-8');
var compiledTemplate = hogan.compile(template);
// configuration ===============================================================

mongoose.connect(configDB.url); // connect to our database
var db = mongoose.connection;
db.once('open', function (callback) {
	require("./tmp36")
});
var addUsers = require("./app/models/addUsers");

require('./config/passport')(passport); // pass passport for configuration

//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 API KEY DO NOT REMOVE
//send emails to all of the users that are aanwezig.
addUsers.find({aanwezig :true},function(err, users){
       for ( var k=0; k < users.length;  k++ ){

	var emailDocent = users[k].emailDocent;
	 var params  = {
     to:        emailDocent,
     from:     'SituationAwareness@example.com',
     subject:  'Emergency, the temprature in the room is higher then 25 degrees',
     html:      compiledTemplate.render()
		};
		var email = new sendgrid.Email(params);

		sendgrid.send(email, function(err, json) {
 		 if (err) { return console.error(err); }
 		 console.log(json);
	});
		module.exports.email = email;
  };
  });
 
module.exports = router;
// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms


app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);