var plotly = require('plotly')('jaapjood', 'pw324hziwz');
var five = require("johnny-five");
var sendgrid  = require('sendgrid')('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');
var board = new five.Board(); 
var mongoose = require('mongoose');
var Temprature = require("./app/models/temprature");
var data = [{x:[], y:[], stream:{token:'7rcnmlwikq', maxpoints:200}}];
var layout = {fileopt : "extend", filename : "tmp36 nodey arduino!"};
var server = require('./server.js');
var hogan = require('hogan.js');
var fs = require('fs');
var telling = 10;
var template = fs.readFileSync('./views/emailTemplate.hjs', 'utf-8');
var compiledTemplate = hogan.compile(template);

board.on("ready", function() {

  // create a new tmp36 sensor object
  var tmp36 = new five.Sensor({
    pin: "A0",
    freq: 1000, // get reading every 1000ms
    thresh: 0.5
  });
  // initialize the plotly graph
  plotly.plot(data,layout,function (err, res) {
    if (err) console.log(err);
    console.log(res);
    //once it's initialized, create a plotly stream
    //to pipe your data!
    var stream = plotly.stream('7rcnmlwikq', function (err, res) {
      if (err) console.log(err);
      console.log(res);
    });


    // this gets called each time there is a new sensor reading
    tmp36.on("data", function() {
      var data = {
        x : getDateString(),
        y : convertTemperature(this.value)
      };

     var temp = new Temprature(data);

        temp.save(function (err, temp) {
        if (err) return console.error(err);

     console.log(temp);

    }); 
        //if data is above cetrain temprature it will send emails.
    if ( data.y > 25) {
      sendgrid.send(server.email, 
      function(err, json) 
          { 
   
    if (err) { return console.error(err); }
      console.log(json);
         });};
      console.log(data);
      // write the data to the plotly stream
      stream.write(JSON.stringify(data)+'\n');

    });
  });

});


// helper function to convert sensor value to temp
function convertTemperature (value) {
  var voltage = value * 0.004882814;
  var celsius = (voltage - 0.5) * 100;
  var fahrenheit = celsius * (9 / 5) + 32;
  return celsius;
}

// little helper function to get a nicely formatted date string
function getDateString () {
  var time = new Date();
  // 14400000 is (GMT-4 Montreal)
  // for your timezone just multiply +/-GMT by 3600000
  var datestr = new Date(time - 14400000).toISOString().replace(/T/, ' ').replace(/Z/, '');
  return datestr;
}
